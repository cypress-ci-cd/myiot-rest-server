from flask import Flask, render_template, jsonify
from flask_cors import CORS
import redis
import settings as sets
import dataset 


app = Flask(__name__)
CORS(app)
_db = dataset.connect(sets.DB)

connection = redis.Redis(host=sets.REDIS_SERVER, 
    port=sets.REDIS_PORT,
    decode_responses=True, 
    db=sets.REDIS_DB_DEFAULT)

connection.ping()

@app.route('/realtime')
def get_temperature():
    return jsonify({'temperature': connection.get('temperature')})

@app.route('/history')
def get_history():
    table = _db.get_table('temperature')
    data = [line for line in table]
    return jsonify(data)

@app.route('/')
def default():
    return render_template('index.html')

def main():
    app.run(host='0.0.0.0', debug=False, port='8000')

if __name__ == '__main__':
    main()
    